#!/usr/bin/env bash
#
# Perform an AWS CodeDeploy deployment to an existing Application and Deployment Group.
#
# Required globals:
#   AWS_ACCESS_KEY_ID
#   AWS_SECRET_ACCESS_KEY
#   AWS_DEFAULT_REGION
#   APPLICATION_NAME
#   COMMAND
#
# Required (upload)
#   ZIP_FILE or ARCHIVE_FILE
#
# Required (deploy)
#   DEPLOYMENT_GROUP
#
# Optional (common)
#   S3_BUCKET
#   VERSION_LABEL
#   DEBUG
#
# Optional (deploy)
#   FILE_EXISTS_BEHAVIOR
#   IGNORE_APPLICATION_STOP_FAILURES
#   WAIT
#   EXTRA_ARGS
#

# Begin Standard 'imports'
source "$(dirname "$0")/common.sh"

set -e
set -o pipefail


# End standard 'imports'

parse_environment_variables() {
  AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?'AWS_ACCESS_KEY_ID variable missing.'}
  AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?'AWS_SECRET_ACCESS_KEY variable missing.'}
  AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:?'AWS_DEFAULT_REGION variable missing.'}
  APPLICATION_NAME=${APPLICATION_NAME:?'APPLICATION_NAME variable missing.'}
  COMMAND=${COMMAND:?'COMMAND variable missing.'}

  S3_BUCKET=${S3_BUCKET:=${APPLICATION_NAME}-codedeploy-deployment}
  VERSION_LABEL_PREFIX=""
  if [[ $S3_BUCKET == */* ]]; then
    VERSION_LABEL_PREFIX="${S3_BUCKET#*/}"
    while [[ "${VERSION_LABEL_PREFIX: -1}" == "/" ]]; do VERSION_LABEL_PREFIX=${VERSION_LABEL_PREFIX:0:-1}; done
    VERSION_LABEL_PREFIX+=/
    VERSION_LABEL_PREFIX=${VERSION_LABEL_PREFIX#/}

    S3_BUCKET=${S3_BUCKET%%/*}
  fi
  VERSION_LABEL=${VERSION_LABEL:=${VERSION_LABEL_PREFIX}${APPLICATION_NAME}-${BITBUCKET_BUILD_NUMBER}-${BITBUCKET_COMMIT:0:8}}

  AWS_DEBUG_ARGS=""
  if [[ "${DEBUG}" == "true" ]]; then
      info "Enabling debug mode."
      AWS_DEBUG_ARGS="--debug"
  fi

  if [[ "${COMMAND}" == "upload" ]] || [[ "${COMMAND}" == "upload-and-deploy" ]]; then
    if [[ -n "${ARCHIVE_FILE}" ]]; then
      filename=${ARCHIVE_FILE,,}
      extension=${filename##*.}
      if [[ "${extension}" == "zip" ]]; then
        BUNDLE_TYPE=zip
      elif [[ "${extension}" == "tar" ]]; then
        BUNDLE_TYPE=tar
      elif [[ "${extension}" == "tgz" ]] || [[ "${extension}" == "gz" ]]; then
        BUNDLE_TYPE=tgz
      else
        fail "Unknown archive type: ${extension}"
      fi
    elif [[ -n "${ZIP_FILE}" ]]; then
      ARCHIVE_FILE=$ZIP_FILE
      BUNDLE_TYPE=zip
    else
      fail "ARCHIVE_FILE and ZIP_FILE are both missing"
    fi

    command_valid=true
  fi

  if [[ "${COMMAND}" == "deploy" ]] || [[ "${COMMAND}" == "upload-and-deploy" ]]; then

    WAIT=${WAIT:="true"}
    DEPLOYMENT_GROUP=${DEPLOYMENT_GROUP:?'DEPLOYMENT_GROUP variable missing.'}
    FILE_EXISTS_BEHAVIOUR=${FILE_EXISTS_BEHAVIOR:='DISALLOW'}
    IGNORE_APPLICATION_STOP_FAILURES=${IGNORE_APPLICATION_STOP_FAILURES:="false"}
    APPLICATION_STOP_FAILURES="--no-ignore-application-stop-failures"
    EXTRA_ARGS=${EXTRA_ARGS:=""}
    if [[ "${IGNORE_APPLICATION_STOP_FAILURES}" == "true" ]]; then
      APPLICATION_STOP_FAILURES="--ignore-application-stop-failures"
    fi
    command_valid=true
  fi

  if [[ ! $command_valid ]]; then
      fail "COMMAND must be either 'upload', 'deploy' or 'upload-and-deploy'"
  fi
}


upload_to_s3() {
  info "Uploading ${ARCHIVE_FILE} to S3."
  run aws s3 cp "${ARCHIVE_FILE}" "s3://${S3_BUCKET}/${VERSION_LABEL}"
  if [[ "${status}" != "0" ]]; then
    fail "Failed to upload ${ARCHIVE_FILE} to S3".
  fi

  info "Registering a revision for the artifact."
  run aws deploy register-application-revision \
    --application-name "${APPLICATION_NAME}" \
    --revision revisionType=S3,s3Location="{bucket=${S3_BUCKET},key=${VERSION_LABEL},bundleType=${BUNDLE_TYPE}}" \
    ${AWS_DEBUG_ARGS}

  if [[ "${status}" == "0" ]]; then
    success "Application uploaded and revision created."
  else
    fail "Failed to register application revision."
  fi
}


deploy() {
  info "Deploying app from revision."

  get_revision_bundle_type

  run aws deploy create-deployment \
      --application-name "${APPLICATION_NAME}" \
      --deployment-group "${DEPLOYMENT_GROUP}" \
      --revision revisionType=S3,s3Location="{bucket=${S3_BUCKET},bundleType=${bundle_type},key=${VERSION_LABEL}}" \
      ${APPLICATION_STOP_FAILURES} \
      --file-exists-behavior "${FILE_EXISTS_BEHAVIOUR}" \
      ${EXTRA_ARGS} \
      ${AWS_DEBUG_ARGS}

  if [[ "${status}" == "0" ]]; then
    deployment_id=$(cat "${output_file}" | jq --raw-output '.deploymentId')
    info "Deployment created with id ${deployment_id}."
  else
    fail "Failed to create deployment."
  fi

  wait_for_deploy
}

get_revision_bundle_type() {
  run aws deploy list-application-revisions \
    --application-name "${APPLICATION_NAME}" \
    --s3-bucket "${S3_BUCKET}" \
    --s3-key-prefix "${VERSION_LABEL}" \
    --sort-by registerTime \
    --sort-order descending

  if [[ "${status}" == "0" ]]; then
    bundle_type=$(cat "${output_file}" | jq --raw-output '.revisions[0].s3Location.bundleType')
  else
    fail "Deployment does not exists."
  fi
}

wait_for_deploy() {
  if [[ "${WAIT}" == "true" ]]; then
    info "Waiting for deployment to complete."
    run aws deploy wait deployment-successful --deployment-id "${deployment_id}" ${AWS_DEBUG_ARGS}

    if [[ "${status}" == "0" ]]; then
      success "Deployment completed successfully."
    else
      error "Deployment failed. Fetching deployment information..."
      run aws deploy get-deployment --deployment-id "${deployment_id}" ${AWS_DEBUG_ARGS}
      exit 1
    fi
  else
    success "Skip waiting for deployment to complete."
  fi
}

parse_environment_variables

if [[ "${COMMAND}" == "upload" ]] || [[ "${COMMAND}" == "upload-and-deploy" ]]; then
  upload_to_s3
fi

if [[ "${COMMAND}" == "deploy" ]] || [[ "${COMMAND}" == "upload-and-deploy" ]]; then
  deploy
fi
