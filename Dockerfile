FROM atlassian/pipelines-awscli:1.16.185

COPY common.sh /
COPY pipe.sh /
COPY LICENSE.txt README.md /

ENTRYPOINT ["/pipe.sh"]
